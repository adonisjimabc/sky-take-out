package com.sky.mapper;

import com.sky.entity.ShoppingCart;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import java.util.List;

@Mapper
public interface ShoppingCartMapper {
    /**
     * @param shoppingCart:
     * @return void
     * @author 17124
     * @description 查询商品是否在购物车
     * @date 2023/6/13 16:23
     */
    ShoppingCart selectByShoppingCart(ShoppingCart shoppingCart);

    /**
     * @param cart:
     * @return void
     * @author 17124
     * @description 更新购物车商品数量
     * @date 2023/6/13 16:51
     */
    @Update("update shopping_cart set number=#{number} where id=#{id}")
    void update(ShoppingCart cart);

    /**
     * @param shoppingCart:
     * @return void
     * @description 向购物车插入数据
     * @date 2023/6/13 16:52
     */
    void insert(ShoppingCart shoppingCart);

    /**
     * @return List<ShoppingCart>
     * @author 17124
     * @description 查询购物车
     * @date 2023/6/13 19:08
     */
    @Select("select * from shopping_cart where user_id =#{userId}")
    List<ShoppingCart> selectList(Long userId);


    /**
     * @param :
     * @return void
     * @description 清空购物车
     * @date 2023/6/13 19:22
     */
    @Delete("delete from shopping_cart where user_id=#{userId}")
    void deleteList(Long userId);

    /**
     * @param cart:
     * @return void
     * @author 17124
     * @description 删除商品
     * @date 2023/6/14 10:54
     */
    @Delete("delete  from shopping_cart where id=#{id}")
    void deleteShoppingCart(ShoppingCart cart);



    //清理购物车中的数据
    @Delete("delete from shopping_cart where user_id=#{userId}")
    void deleteByUserId(Long userId);


    //查询当前用户的购物车数据
    @Select("select * from shopping_cart where user_id =#{userId}")
    List<ShoppingCart> list(ShoppingCart shoppingCart);

    /**
     * 批量插入购物车数据
     *
     * @param shoppingCartList
     */
    void insertBatch(List<ShoppingCart> shoppingCartList);
}
