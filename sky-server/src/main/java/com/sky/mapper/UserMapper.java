package com.sky.mapper;

import com.sky.entity.User;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Select;

@Mapper
public interface UserMapper {

    /**
     * @param openid:
     * @return User
     * @author 17124
     * @description 根据openid查询是否为新用户
     * @date 2023/6/12 16:50
     */
    @Select("select * from user where openid=#{openid}")
    User getByOpenid(String openid);

    /**
     * @param user:
     * @return void
     * @author 17124
     * @description 插入新用户
     * @date 2023/6/12 16:51
     */
    @Insert("insert into user (openid, name, phone, sex, id_number, avatar, create_time)" +
            "values (#{openid}, #{name}, #{phone}, #{sex}, #{idNumber}, #{avatar}, #{createTime})")
    //Controller层会使用到userId
    @Options(useGeneratedKeys = true,keyProperty = "id")
    void insert(User user);

    User getById(Long userId);
}
