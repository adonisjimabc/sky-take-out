package com.sky.service.impl;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.sky.constant.MessageConstant;
import com.sky.constant.StatusConstant;
import com.sky.dto.DishDTO;
import com.sky.dto.DishPageQueryDTO;
import com.sky.entity.Dish;
import com.sky.entity.DishFlavor;
import com.sky.exception.BaseException;
import com.sky.mapper.DishMapper;
import com.sky.mapper.FlavorsMapper;
import com.sky.result.PageResult;
import com.sky.result.Result;
import com.sky.service.DishService;
import com.sky.vo.DishVO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;


@Service

public class DishSereviceImpl implements DishService {

    @Autowired
    private DishMapper dishMapper;

    @Autowired
    private FlavorsMapper flavorsMapper;

    /*@Autowired
    private RedisTemplate redisTemplate;*/


    @Override
    @Transactional(rollbackFor = Exception.class)
    public void saveDishWithFlavor(DishDTO dishDTO) {
        //清理缓存数据
        String key = "dish_" + dishDTO.getCategoryId();
        //cleanCache(key);

        //先将数据加入到菜品表
        Dish dish = new Dish();
        BeanUtils.copyProperties(dishDTO, dish);
        dishMapper.insert(dish);

        List<DishFlavor> flavors = dishDTO.getFlavors();
        if (flavors != null && flavors.size() > 0) {
            //口味数据加入到口味表
            flavors.forEach(dishFlavor -> dishFlavor.setDishId(dish.getId()));
            flavorsMapper.insertBatch(flavors);
        }
    }

    @Override
    public PageResult page(DishPageQueryDTO dishPageQueryDTO) {
        PageHelper.startPage(dishPageQueryDTO.getPage(), dishPageQueryDTO.getPageSize());

        Page<DishVO> page = dishMapper.page(dishPageQueryDTO);

        return new PageResult(page.getTotal(), page.getResult());
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void deleteByIds(List<Long> ids) {

        //将所有的菜品缓存数据清理掉，所有以dish_开头的key
        //cleanCache("dish_*");

        //在售的商品不能删除
        Integer numbersWithSale = dishMapper.countsStatusWithSale(ids);
        if (numbersWithSale > 0) {
            throw new BaseException(MessageConstant.SETMEAL_ON_SALE);
        }
        //关联套餐的菜品不能删除
        Integer numbersWithContactSetmeal = dishMapper.countsStatusWithSetmeal(ids);
        if (numbersWithContactSetmeal > 0) {
            throw new BaseException(MessageConstant.CATEGORY_BE_RELATED_BY_SETMEAL);
        }
        //删除商品
        dishMapper.deleteByIds(ids);
        //删除商品的口味信息
        flavorsMapper.deleteByIds(ids);

    }

    @Override
    public DishVO selectDishById(Long id) {
        DishVO dishVO = dishMapper.selectDishById(id);
        List<DishFlavor> dishFlavor = flavorsMapper.selectByDishId(id);
        dishVO.setFlavors(dishFlavor);
        return dishVO;
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void update(DishDTO dishDTO) {

        //将所有的菜品缓存数据清理掉，所有以dish_开头的key
        //cleanCache("dish_*");


        Long id = dishDTO.getId();
        List<Long> list = new ArrayList<>();
        list.add(id);
        dishMapper.deleteByIds(list);
       /* Dish dish = new Dish();
        BeanUtils.copyProperties(dishDTO, dish);
        //先把菜品信息加入，再加入菜品口味信息
        dishMapper.insert(dish);
        List<DishFlavor> flavors = dishDTO.getFlavors();*/
        //直接调用已经写好的添加菜品方法
        saveDishWithFlavor(dishDTO);
    }

    @Override
    public List<Dish> selectDishByCategoryId(Long id) {
        Dish dish = Dish.builder()
                .categoryId(id)
                .status(StatusConstant.ENABLE)
                .build();
        List<Dish> list = dishMapper.selectDishByCategoryId(dish);
        return list;
    }

    @Override
    public void saleOrNot(Integer status, Long id) {

        //将所有的菜品缓存数据清理掉，所有以dish_开头的key
        //cleanCache("dish_*");
        dishMapper.updateSaleOrNot(status, id);
    }


    @Override
    public List<DishVO> listwithFlavor(Dish dish) {

        List<Dish> list = dishMapper.selectDishByCategoryId(dish);
        //遍历菜品集合，加入口味数据
        List<DishVO> dishVOS = new ArrayList<>();
        for (Dish dish1 : list) {
            Long id = dish1.getId();
            DishVO dishVO = new DishVO();
            BeanUtils.copyProperties(dish1, dishVO);
            List<DishFlavor> dishFlavors = flavorsMapper.selectByDishId(id);
            dishVO.setFlavors(dishFlavors);
            dishVOS.add(dishVO);
        }
        return dishVOS;
    }
    /**
     * 清理缓存数据
     * @param pattern
     */
    /*private void cleanCache(String pattern){
        Set keys = redisTemplate.keys(pattern);
        redisTemplate.delete(keys);
    }*/
}
