package com.sky.service;

import com.sky.dto.UserLoginDTO;
import com.sky.entity.User;

/**
 * @projectName: sky-take-out
 * @package: com.sky.service
 * @className: UserService
 * @author: ZZH
 * @description: TODO
 * @date: 2023/6/12 16:11
 * @version: 1.0
 */
public interface UserService {

    /**
     * 微信登录
     * @param userLoginDTO
     * @return
     */
    User wxLogin(UserLoginDTO userLoginDTO);
}
